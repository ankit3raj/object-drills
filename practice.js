const school = {
  "engineering": {
    "students": 324,
    "dep_id": 3,
    "faculties": 4
  },
  "medical": {
    "students": 499,
    "dep_id": 2,
    "faculties": 6
  },
  "pure-science": {
    "students": 133,
    "dep_id": 1,
    "faculties": 2
  },
  "linguistics": {
    "students": 183,
    "dep_id": 4,
    "faculties": 3
  },
  "philosophy": {
    "students": 73,
    "dep_id": 5,
    "faculties": 2
  }
}
/*

    1. Group the data in terms of deparment id.
    2. Filter data with students greater than 200.
    3. Add a new property (labs-required) to each object . Set value as true for engineering medical and purescience ..and false for lingustics and philosophy

*/

//1
const result = Object.keys(school).reduce((acc,curr) => {
  const {dep_id,students,faculties} = school[curr];
  if(curr[dep_id]){
      acc[dep_id] = {[curr]:{
        dep_id:dep_id,
        students:students,
        faculties:faculties
      }}
  }else{
      acc[dep_id] ={};
      acc[dep_id] = {[curr]:{
        dep_id:dep_id,
        students:students,
        faculties:faculties
      }}
  }
  return acc;
},{})
// console.log(result);

//2

const result1 = Object.keys(school).reduce((acc,curr) => {
    if(school[curr].students>200){
        acc[curr] = school[curr]
    }
    return acc;

},{});
// console.log(result1);

//3 
const result3 = Object.keys(school).reduce((acc,curr) => {
  const {dep_id,students,faculties} = school[curr];
  if(curr == "engineering" || curr =="medical" || curr == "pure-science"){
      acc[curr] = {
        dep_id:dep_id,
        students:students,
        faculties:faculties,
        labsRequired:true

      }
  }else{
      acc[curr] ={};
      acc[curr] = {
        dep_id:dep_id,
        students:students,
        faculties:faculties,
        labsRequired: false
      }
  }
  return acc;
}, {})

console.log(result3)





